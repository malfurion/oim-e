export default class Page {
    public size: number = 30;
    public totalCount: number = 0;
    public number: number = 1;
    public totalPage: number = 0;
}
